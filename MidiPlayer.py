#!/usr/bin/env python
import mido
import time
from mido.ports import MultiPort

trackmapping = None


def strapinputs():
    inputers = []

    while 0 == len(inputers):
        print "Connecting..."
        for inputName in mido.get_input_names():
            inputers.append(mido.open_input(inputName))
        if len(inputers) == 0:
            raise UserWarning("MIDI Inputs are not found")

    print "Connected."
    return MultiPort(inputers)


def play(trackname):
    print trackname
    for message in mido.MidiFile(trackname).play():
        print message


def perform(message):
    if hasattr(message, 'note') and message.type == 'note_on':
        trackname = trackmapping.get(message.note, 'default.mid')
        print trackname


def listen():
    inputs = strapinputs()
    with inputs as port:
        for message in port:
            perform(message)


def virtualoutput():
    rtmidi = mido.Backend('mido.backends.rtmidi')
    minput = rtmidi.open_input('AdashoreIn', virtual=True)
    outport = rtmidi.open_output('AdashoreOut', virtual=True)
    with minput as port:
        for message in port:
            print message