import MidiPlayer
track = 'Konstantine.mid'

MidiPlayer.trackmapping = {48: 'Konstantine.mi', 50: 'b.mid', 52: 'c.mid'}
try:
    MidiPlayer.listen()
except KeyboardInterrupt:
    print "Stopping listener"
